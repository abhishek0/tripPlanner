export interface IUserLogin {
  email: string;
  password: string;
}

export interface IUser {
  id?: number;
  name: string;
  email: string;
  role: string;
  password: string;
}

export enum EUser {
  Name = 'name',
  Email = 'email',
  Role = 'role',
  Password = 'password'
}

export enum EUserRole {
  'Regular' = 'regular',
  'Manager' = 'manager',
  'Admin' = 'admin'
}

export type LoggedIn = 'success' | 'failed' | 'notattempted';
