export interface ICreateTrip {
  destination: string;
  startDate: string;
  endDate: string;
  comment?: string;
}

export interface ITrip {
  comment?: string;
  destination: string;
  endDate: string;
  id: number;
  startDate: string;
  userId: number;
}
