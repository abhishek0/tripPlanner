import awaitTo from "async-await-error-handling";
import axios from "axios";
import Endpoints from "./Endpoints";
import {ICreateTrip, ITrip} from "../models/trip.models";

export default class TripService {
  static async createTrip(tripData: ICreateTrip): Promise<[ITrip, string]> {
    const [err, data] = await awaitTo(
      axios.post(Endpoints.tripUrl, tripData, {
        headers: Endpoints.headers()
      })
    );

    if(!err) return [data.data.data as ITrip, ''];
    return [undefined, Endpoints.getError(err)];
  }

  static async getTrips(param = {}): Promise<[ITrip[], string]> {
    const [err, data] = await awaitTo(
      axios.get(Endpoints.tripUrl, {
        headers: Endpoints.headers(),
        params: param
      })
    );

    if(!err) return [data.data.data as ITrip[], ''];
    return [[] as ITrip[], Endpoints.getError(err)];
  }

  static async deleteTrip(id): Promise<[ITrip, string]> {
    const [err, data] = await awaitTo(
      axios.delete(`${Endpoints.tripUrl}/${id}`, {
        headers: Endpoints.headers()
      })
    );

    if(!err) return [data.data.data as ITrip, ''];
    return [undefined as ITrip, Endpoints.getError(err)];
  }

  static async updateTrip(id, formData): Promise<[ITrip, string]> {
    const [err, data] = await awaitTo(
      axios.put(`${Endpoints.tripUrl}/${id}`, formData, {
        headers: Endpoints.headers()
      })
    );

    if(!err) return [data.data.data as ITrip, ''];
    return [undefined as ITrip, Endpoints.getError(err)];
  }
}
