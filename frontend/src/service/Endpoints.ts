class Endpoints {
  static basePath = 'http://localhost:5000/api/v1';

  static users = `${Endpoints.basePath}/user`;

  static loginUrl = `${Endpoints.users}/login`;

  static signupUrl = `${Endpoints.users}/signup`;

  static tripUrl = `${Endpoints.basePath}/trip`;

  static getError = (err: any) => {
    if (err.response) {
      if (err.response.status === 500) {
        return 'Internal Server Error';
      }
      if (err.response.status === 404) {
        return 'Not found';
      }
      // if (err.response.status === 403) {
      //   UserAction.logoutUser().then(() => {
      //     return 'Permission Denied';
      //   });
      // }
      if (err.response.status === 401) {
        return 'Unauthorized Access';
      }
      if (err.response.status === 400) {
        if (err.response.data) return err.response.data.message;
      }
      // return oc(err).response.data.errorMessage('Something went wrong');
    }
    return 'Something went wrong';
  };

  static headers = () => ({
    jwttoken: localStorage.getItem('jwt')
  });

}

export interface IEndPoint {
  url: string;
  method: 'POST' | 'GET' | 'DELETE' | 'PUT';
}

export default Endpoints;
