import awaitTo from "async-await-error-handling";
import axios from "axios";
import Endpoints from "./Endpoints";
import {IUser} from "../models/user.models";

export default class UserService {
  static async loginUser(
    email: string,
    password: string
  ): Promise<[IUser, string, string]> {
    const [err, data] = await awaitTo(
      axios.post(Endpoints.loginUrl, {
        email,
        password
      })
    );

    if(!err) return [data.data.data as IUser, data.data.data.jwtToken, ''];
    return [undefined as IUser, '', Endpoints.getError(err)];
  }

  static async userSignUp(
    email: string,
    password: string,
    role: string,
    name: string
  ): Promise<[IUser, string]> {
    const [err, data] = await awaitTo(
      axios.post(Endpoints.signupUrl, {
        email,
        password,
        role,
        name
      })
    );

    if(!err) return [data.data.data as IUser, ''];
    return [undefined as IUser, Endpoints.getError(err)];
  }

  static async getUsers(param = {}): Promise<[IUser[], string]> {
    const [err, data] = await awaitTo(
      axios.get(Endpoints.users, {
        headers: Endpoints.headers(),
        params: param
      })
    );

    if(!err) return [data.data.data as IUser[], ''];
    return [undefined as IUser[], Endpoints.getError(err)];
  }

  static async deleteUser(id): Promise<[IUser, string]> {
    const [err, data] = await awaitTo(
      axios.delete(`${Endpoints.users}/${id}`, {
        headers: Endpoints.headers()
      })
    );

    if(!err) return [data.data.data as IUser, ''];
    return [undefined as IUser, Endpoints.getError(err)];
  }

  static async updateUser(id, formData): Promise<[IUser, string]> {
    const [err, data] = await awaitTo(
      axios.put(`${Endpoints.users}/${id}`, formData, {
        headers: Endpoints.headers()
      })
    );

    if(!err) return [data.data.data as IUser, ''];
    return [undefined as IUser, Endpoints.getError(err)];
  }
};
