import React from 'react';
import { useSelector } from 'react-redux';
import { RouteProps } from 'react-router';
import { Redirect, Route } from 'react-router-dom';
import { RootState } from '../store';
import { Routes } from './Routes';
import {LoadingComponent} from "../components/Loading";
import {IUser} from "../models/user.models";

export interface DispatchState {
  loggedIn: 'success' | 'failed' | 'notattempted';
  loggedInUser: IUser;
  isLoading: boolean;
}

interface IOwnProps extends RouteProps {
  Component: any;
  path?: string;
}

const ProtectedRoute: React.FunctionComponent<IOwnProps> = ({
  Component,
  path
}) => {
  const { loggedIn, loggedInUser, isLoading } = useSelector<
    RootState,
    DispatchState
    >(({ user: { loggedIn, loggedInUser, isLoading } }) => {
    return {
      loggedIn,
      loggedInUser,
      isLoading
    };
  });

  if (loggedIn === 'notattempted') {
    return <Redirect to={Routes.login} />;
  }
  if (isLoading) {
    return <LoadingComponent />;
  }
  if (loggedIn === 'success') {
    return <Route path={path} render={(props) => <Component {...props} />} />;
  }
  if (loggedIn === 'failed') {
    return <Redirect to={Routes.login} />;
  }
  return <div />;
};

export default ProtectedRoute;
