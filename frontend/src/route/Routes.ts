export enum Routes {
  home = '/',
  login = '/login',
  signUp = '/signUp',
  users = '/users',
  trips = '/trips',
  createTrip = '/trips/create'
}
