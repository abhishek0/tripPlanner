import React, {useState, useEffect} from 'react';
import {ITrip} from "../models/trip.models";
import TripCard from "./TripCard";
import TripService from "../service/TripService";
import {useSelector} from "react-redux";
import {RootState} from "../store";
import {DispatchState} from "../route/ProtectedRoute";
import {Button, Col, Form, Row, DatePicker} from "antd";
import {Routes} from "../route/Routes";
import Loader from "./Loader";
import moment from "moment";

const queryString = require('query-string');

const TripContainer = (props) => {
  const { loggedIn, loggedInUser, isLoading } = useSelector<
    RootState,
    DispatchState
    >(({ user: { loggedIn, loggedInUser, isLoading } }) => {
    return {
      loggedIn,
      loggedInUser,
      isLoading
    };
  });

  const [form] = Form.useForm();
  const [tripData, setTripData] = useState([] as ITrip[]);
  const [loader, setLoader] = useState(true);

  const getValueFromProps = props.location
    ? queryString.parse(props.location.search)
    : {};

  const [page, setPage] = useState<number>(
    getValueFromProps.page ? +getValueFromProps.page : 0
  );

  const [queString, setQueString] = useState(getValueFromProps);

  const getTripData = async () => {
    const [data, err] = await TripService.getTrips(queString);
    if (!err) {
      setTripData(data);
    } else {
      setTripData([]);
    }
    setLoader(false);
  };

  const onFinish = async (values) => {
    const obj = {};
    Object.keys(values).forEach((d) => {
      if (values[d]) obj[d] = values[d];
    });

    Object.keys(obj).forEach((key) => {
      obj[key] = moment(obj[key]).format('YYYY-MM-DD');
    });

    props.history.push({
      pathname: Routes.trips,
      search: queryString.stringify({ ...obj })
    });
    setQueString({ ...obj });
  };

  useEffect(() => {
    (async () => {
      await getTripData();
    })();
  }, [queString]);

  const Filters = () => {
    return (
      <div style={{paddingLeft: 100}}>
        <Form
          form={form}
          name="filter"
          onFinish={onFinish}
          preserve>
          <Row gutter={24}>
            <Col span={9}>
              <Form.Item
                name={'min_start_date'}
                label={'Start Date Starts'}
                rules={[
                  {
                    type: 'object',
                    required: false,
                  }
                ]}>
                <DatePicker style={{width: '100%'}} format={'YYYY-MM-DD'} />
              </Form.Item>
            </Col>
            <Col span={9}>
              <Form.Item
                name={'max_start_date'}
                label={'Start Date Ends'}
                rules={[
                  {
                    type: 'object',
                    required: false,
                  }
                ]}>
                <DatePicker style={{width: '100%'}} format={'YYYY-MM-DD'} />
              </Form.Item>
            </Col>
            <Col span={9}>
              <Form.Item
                name={'min_end_date'}
                label={'End Date Starts'}
                rules={[
                  {
                    type: 'object',
                    required: false,
                  }
                ]}>
                <DatePicker style={{width: '100%'}} format={'YYYY-MM-DD'} />
              </Form.Item>
            </Col>
            <Col span={9}>
              <Form.Item
                name={'max_end_date'}
                label={'End Date Ends'}
                rules={[
                  {
                    type: 'object',
                    required: false,
                  }
                ]}>
                <DatePicker style={{width: '100%'}} format={'YYYY-MM-DD'} />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Row>
        </Form>
        <br />
      </div>
    );
  };

  const onClickNext = () => {
    props.history.push({
      pathname: Routes.trips,
      search: queryString.stringify({ ...queString, page: +page + 1 })
    });
    setLoader(true);
    setQueString({ ...queString, page: +page + 1 });
    setPage(+page + 1);
  };

  const onClickPrev = () => {
    props.history.push({
      pathname: Routes.trips,
      search: queryString.stringify({ ...queString, page: +page - 1 })
    });
    setLoader(true);
    setPage(+page - 1);
    setQueString({ ...queString, page: +page - 1 });
  };

  if (loader) return <Loader />;

  if(tripData?.length === 0) return (
    <div>
      <h1>Trip Container</h1>

      <h4>Data not found!!!</h4>

      <div>
        <Button disabled={page === 0} onClick={onClickPrev}>
          Previous
        </Button>
        &nbsp;
        <Button disabled onClick={onClickNext}>Next</Button>
      </div>
    </div>
  );

  return (
    <div>
      <h1>Trip Container</h1>

      <Filters />

      {
        tripData.map(d => (
          <TripCard
            key={d.id}
            tripData={d}
            userRole={loggedInUser.role}
            updateTripData={getTripData}
          />
        ))
      }

      <div style={{paddingBottom: 50}}>
        <Button disabled={page === 0} onClick={onClickPrev}>
          Previous
        </Button>
        &nbsp;
        <Button onClick={onClickNext}>Next</Button>
      </div>
    </div>
  );
};

export default TripContainer;
