import React, {memo} from 'react';
import { Menu } from "antd";
import { useHistory } from 'react-router';
import UserAction from "../store/user/UserAction";
import {toast} from "react-toastify";
import '../css/navbar.css';
import {
  UserAddOutlined,
  LoginOutlined,
  HomeOutlined,
  LogoutOutlined
} from '@ant-design/icons';
import {Routes} from "../route/Routes";
import {useSelector} from "react-redux";
import {RootState} from "../store";
import {LoggedIn} from "../models/user.models";

const Navbar = () => {
  const history = useHistory();
  const loggedIn = useSelector<RootState, LoggedIn>(
    (state) => state.user.loggedIn
  );

  const logoutUser = async () => {
    const [message, err] = await UserAction.logoutUser();
    history.push(Routes.login);
    if(!err) {
      toast.success(message);
    }
  }

  return (
    <div className='navContainer'>
      <Menu mode="horizontal" theme={'dark'}>
        <Menu.Item
          key={Routes.home}
          onClick={() => history.push(Routes.home)}
          icon={<HomeOutlined />}>
          TRIPBSD
        </Menu.Item>

        {loggedIn === 'success' ? (
          <>
            <Menu.Item
              key={Routes.trips}
              onClick={() => history.push(Routes.trips)}
              icon={<UserAddOutlined />}>
              Show Trips
            </Menu.Item>

            <Menu.Item
              key={'users'}
              onClick={() => history.push(Routes.users)}
              icon={<UserAddOutlined />}>
              Users
            </Menu.Item>

            <Menu.Item
              key={Routes.createTrip}
              onClick={() => history.push(Routes.createTrip)}
              icon={<UserAddOutlined />}>
              Create Trip
            </Menu.Item>

            <Menu.Item
              key={'logout'}
              onClick={logoutUser}
              icon={<LogoutOutlined />}>
              Logout
            </Menu.Item>
          </>
        ): (
          <>
          <Menu.Item
            key={Routes.login}
            onClick={() => history.push(Routes.login)}
            icon={<LoginOutlined />}>
            Login
          </Menu.Item>

          <Menu.Item
            key={Routes.signUp}
            onClick={() => history.push(Routes.signUp)}
            icon={<UserAddOutlined />}>
            SignUp
          </Menu.Item>
          </>
        )}
      </Menu>
    </div>
  );
};

export default memo(Navbar);
