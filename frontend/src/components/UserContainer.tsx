import React, { useState, useEffect } from 'react';
import { IUser } from "../models/user.models";
import UserService from "../service/UserService";
import UserCard from "./UserCard";
import {useSelector} from "react-redux";
import {RootState} from "../store";
import {DispatchState} from "../route/ProtectedRoute";
import Loader from "./Loader";
import {Routes} from "../route/Routes";
import {Button} from "antd";
import {toast} from "react-toastify";

const queryString = require('query-string');

const UserContainer = (props) => {
  const { loggedIn, loggedInUser, isLoading } = useSelector<
    RootState,
    DispatchState
    >(({ user: { loggedIn, loggedInUser, isLoading } }) => {
    return {
      loggedIn,
      loggedInUser,
      isLoading
    };
  });

  const [userData, setUserData] = useState([] as IUser[]);
  const [loader, setLoader] = useState(true);

  const getValueFromProps = props.location
    ? queryString.parse(props.location.search)
    : {};

  const [page, setPage] = useState<number>(
    getValueFromProps.page ? +getValueFromProps.page : 0
  );

  const [queString, setQueString] = useState(getValueFromProps);

  const getUserData = async () =>  {
    if (loggedInUser && loggedInUser.role === 'regular') {
      toast.info('Regular users cannot view other users');
      return;
    }
    const [data, err] = await UserService.getUsers(queString);
    if (data) {
      setUserData(data);
      setLoader(false);
    } else {
      setUserData([]);
      setLoader(false);
    }
  }

  useEffect(() => {
    (async () => {
      await getUserData();
    })();
  }, [queString]);

  const onClickNext = () => {
    props.history.push({
      pathname: Routes.users,
      search: queryString.stringify({ ...queString, page: +page + 1 })
    });
    setLoader(true);
    setQueString({ ...queString, page: +page + 1 });
    setPage(+page + 1);
  };

  const onClickPrev = () => {
    props.history.push({
      pathname: Routes.users,
      search: queryString.stringify({ ...queString, page: +page - 1 })
    });
    setLoader(true);
    setPage(+page - 1);
    setQueString({ ...queString, page: +page - 1 });
  };

  if(loggedInUser && loggedInUser.role === 'regular') {
    return (
      <div>
        <h1>User Container</h1>
        <p>Regular users cannot view other users profile...</p>
      </div>
    )
  }

  if (loader) return <Loader />;

  if(userData?.length === 0) return (
    <div>
      <h1>User Container</h1>

      <h4>Data not found!!!</h4>

      <div>
        <Button disabled={page == 0} onClick={onClickPrev}>
          Previous
        </Button>
        &nbsp;
        <Button disabled onClick={onClickNext}>Next</Button>
      </div>
    </div>
  );

  return (
    <div>
      <h1>User Container</h1>
      {
        userData.map(d => (
          <UserCard
            userData={d}
            key={d.id}
            userRole={loggedInUser.role}
            updateUserData={getUserData}
          />
        ))
      }

      <div style={{paddingBottom: 50}}>
        <Button disabled={page == 0} onClick={onClickPrev}>
          Previous
        </Button>
        &nbsp;
        <Button onClick={onClickNext}>Next</Button>
      </div>
    </div>
  )
}

export default UserContainer;
