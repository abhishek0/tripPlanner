import React, {useEffect} from 'react';
import {Button, Form, Input, DatePicker} from "antd";
import moment from 'moment';
import TripService from "../service/TripService";
import {toast} from "react-toastify";

const CreateTripContainer = ({
  edit = false,
  formInitialData,
  onSubmitForm
}) => {
  const [form] = Form.useForm();

  useEffect(() => {
    if(edit) {
      formInitialData['startDate'] = moment(formInitialData['startDate'])
      formInitialData['endDate'] = moment(formInitialData['endDate'])

      form.setFieldsValue({
        ...formInitialData
      });
    }
  }, []);

  const onFinish = async (values) => {
    values['startDate'] = moment(
      values.startDate
    ).format('YYYY-MM-DD');
    values['endDate'] = moment(
      values.endDate
    ).format('YYYY-MM-DD');

    if(edit) {
      onSubmitForm(values);
    } else {
      const [data, err] = await TripService.createTrip(values);
      if(data) {
        toast.success('Trip created successfully!!!');
      } else {
        toast.error(err);
      }
    }
  }

  const disabledDate = (current) => {
    return !(
      current >= moment('2021-01-01', 'YYYY-MM-DD') &&
      current <= moment('2099-12-31', 'YYYY-MM-DD')
    );
  }

  return (
    <div className='formContainer' style={{paddingTop: '10vh'}}>
      <h1>Create Trip</h1>
      <Form
        form={form}
        name="createTripForm"
        className="login-form"
        initialValues={{ remember: false }}
        onFinish={onFinish}
      >
        <Form.Item
          label="Destination"
          name="destination"
          rules={[{ required: true, message: 'Please enter your destination!!!' }]}
        >
          <Input
            // prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="Where do you want to go?"
          />
        </Form.Item>

        <Form.Item
          name="startDate"
          label="Start Date"
          rules={[
            {
              type: 'object',
              required: true,
              message: 'Please select starting date!'
            }
          ]}>
          <DatePicker
            format="YYYY-MM-DD"
            disabledDate={disabledDate}
            showTime={{ defaultValue: moment() }}
          />
        </Form.Item>

        <Form.Item
          name="endDate"
          label="End Date"
          rules={[
            {
              type: 'object',
              required: true,
              message: 'Please select ending date!'
            }
          ]}>
          <DatePicker
            format="YYYY-MM-DD"
            disabledDate={disabledDate}
            showTime={{ defaultValue: moment() }}
          />
        </Form.Item>

        <Form.Item
          label="Comment"
          name="comment"
        >
          <Input
            // prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="Enter comment..."
          />
        </Form.Item>

        <Form.Item className='formItem'>
          <Button type="primary" htmlType="submit" className="login-form-button">
            Create
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default CreateTripContainer;
