import React, {useState} from 'react';
import {Card, Button, Modal} from "antd";
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import '../css/base.css';
import UserService from "../service/UserService";
import Signup from "./Signup";
import {toast} from "react-toastify";

const UserCard = (props) => {
  const {userData, userRole, updateUserData} = props;
  const [tab, setTab] = useState('id');
  const [openModal, setOpenModal] = useState(false);

  const toggleModal = () => {
    setOpenModal(!openModal);
  }

  const UserProperties = [
    'id',
    'name',
    'email',
    'role',
  ];

  const tabList = [
    {
      key: UserProperties[0],
      tab: 'Id',
    },
    {
      key: UserProperties[1],
      tab: 'Name',
    },
    {
      key: UserProperties[2],
      tab: 'Email',
    },
    {
      key: UserProperties[3],
      tab: 'Role',
    }
  ];

  const contentList = {
    id: <p>{userData.id}</p>,
    name: <p>{userData.name}</p>,
    email: <p>{userData.email}</p>,
    role: <p>{userData.role}</p>,
  };

  const onTabChange = (key) => {
    setTab(key);
  };

  const onFormUpdate = async (formData) => {
    const [data, err] = await UserService.updateUser(userData.id, formData);
    if(data) {
      updateUserData();
      setOpenModal(false);
      toast.success('User updated successfully!!!');
    } else {
      toast.error(err);
    }
  }

  const deleteUser = async () => {
    const [data, err] = await UserService.deleteUser(userData.id);
    if(data) {
      await updateUserData();
      toast.success('User deleted successfully');
    } else {
      toast.error(err);
    }
  }

  const editDeleteCondition = () => {
    return (
      userRole !== 'regular' &&
      (
        userRole === 'admin' ||
        userData.role !== 'admin'
      )
    );
  }

  return (
    <div className='shadow-card'>
      <Card
        key={userData.id}
        style={{ width: '40%', backgroundColor: '#E0E0E0', borderRadius: 10}}
        title={<strong>{userData.name}</strong>}
        onTabChange={onTabChange}
        extra={
          ( editDeleteCondition() ?
            <>
              <Button
                type="primary"
                onClick={toggleModal}
                icon={<EditOutlined/>}>
                Edit
              </Button>
              &nbsp;
              <Button
                danger
                type="primary"
                onClick={deleteUser}
                icon={<DeleteOutlined />}>
                Delete
              </Button>
            </> : null
          )
        }
        tabList={tabList}>
        {contentList[tab]}
      </Card>

      <Modal
        title="Edit User Details"
        visible={openModal}
        onOk={toggleModal}
        footer={null}
        onCancel={toggleModal}
        okButtonProps={{ disabled: true }}
        cancelButtonProps={{ disabled: true }}>
        <Signup
          edit={true}
          formInitialData={userData}
          onSubmitForm={onFormUpdate}
        />
      </Modal>
    </div>
  );
};

export default UserCard;
