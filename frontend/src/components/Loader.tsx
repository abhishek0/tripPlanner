import { Spin, Space } from 'antd';
import React from 'react';

const Loader = () => (
  <div style={{paddingTop: 20}}>
    <Space size="middle">
      <Spin size="large" />
    </Space>
  </div>
);

export default Loader;
