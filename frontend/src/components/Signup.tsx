import React, {useEffect} from 'react';
import {Button, Form, Input, Select} from "antd";
import {Link} from "react-router-dom";
import {EUser, EUserRole, IUser} from "../models/user.models";
import UserService from "../service/UserService";
import {useSelector} from "react-redux";
import {RootState} from "../store";
import {DispatchState} from "../route/ProtectedRoute";
import {toast} from "react-toastify";

const { Option } = Select;

const Signup = ({
  edit = false,
  formInitialData,
  onSubmitForm
}) => {
  const { loggedIn, loggedInUser, isLoading } = useSelector<
    RootState,
    DispatchState
    >(({ user: { loggedIn, loggedInUser, isLoading } }) => {
    return {
      loggedIn,
      loggedInUser,
      isLoading
    };
  });
  const [form] = Form.useForm();

  useEffect(() => {
    if(edit) {
      form.setFieldsValue({
        ...formInitialData
      });
    }
  }, []);

  const onFinish = async (values: IUser) => {
    if(edit) {
      onSubmitForm(values);
    } else {
      const { name, email, password, role } = values;
      const [data, err] = await UserService.userSignUp(
          email,
          password,
          role,
          name
      );

      if(data) {
        toast.success('Signup successful!!!');
      } else {
        toast.error(err);
      }
    }
  }
  return (
    <div className='formContainer' style={{paddingTop: '10vh'}}>
      {edit ? <h1>Edit User Details</h1> : <h1>Sign Up</h1>}
      <Form
        form={form}
        name="signUpForm"
        className="login-form"
        initialValues={{ remember: true }}
        onFinish={onFinish}
      >
        <Form.Item
          label="Name"
          name="name"
          rules={[{ required: true, message: 'Please enter your name!!!' }]}
        >
          <Input
            // prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="Enter name..."
          />
        </Form.Item>

        <Form.Item
          label="Email"
          name="email"
          rules={[{ required: true, message: 'Please enter your email!!!' }]}
        >
          <Input
            type={"email"}
            placeholder="Enter email..."
          />
        </Form.Item>

        {!edit ? (
            <Form.Item
            name="password"
            label='Pass'
            rules={[{required: true, message: 'Please enter your Password!!!'}]}>
              <Input
                // prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                placeholder="Enter password..."
              />
            </Form.Item>
          ) : null
        }

        <Form.Item name={EUser.Role} label="Role" initialValue={EUserRole.Regular}>
          <Select
            style={{ width: '100%' }}
            placeholder="Please select"
            disabled={loggedInUser?.role !== 'admin'}>
            <Option key={EUserRole.Regular} value={EUserRole.Regular}>Regular</Option>
            <Option key={EUserRole.Manager} value={EUserRole.Manager}>Manager</Option>
            <Option key={EUserRole.Admin} value={EUserRole.Admin}>Admin</Option>
          </Select>
        </Form.Item>

        <Form.Item className='formItem'>
          <Button type="primary" htmlType="submit" className="login-form-button">
            {edit ? 'Update User' : 'Register'}
          </Button>
          <br />
          <br />
          {!edit ?
            <>
              Already have an account?
              <Link to='/login'>Log In!</Link>
            </> : null
          }
        </Form.Item>
      </Form>
    </div>
  );
};

export default Signup;
