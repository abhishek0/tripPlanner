import React from 'react';
import { Form, Input, Button } from "antd";
import { Link, useHistory } from 'react-router-dom';
import '../css/loginContainer.css';
import {
  UserOutlined,
  LockOutlined
} from '@ant-design/icons';
import {useSelector} from "react-redux";
import {Routes} from "../route/Routes";
import {RootState} from "../store";
import {IUserLogin, LoggedIn} from "../models/user.models";
import UserAction from "../store/user/UserAction";
import {toast} from "react-toastify";

const Login =  () => {
  const [form] = Form.useForm();
  const loggedIn = useSelector<RootState, LoggedIn>(
    (state) => state.user.loggedIn
  );
  const history = useHistory();

  if (loggedIn === 'success') {
    history.replace(Routes.trips);
  }

  const onFinish = async (values: IUserLogin) => {
    const {email, password} = values;
    const [data, err] = await UserAction.loginUser(email, password);

    if(data) {
      toast.success('Logged in successfully!!!');
    } else {
      toast.error(err);
    }
  }

  return (
    <div className='formContainer' style={{paddingTop: '10vh'}}>
      <h1>Login</h1>
      <Form
        form={form}
        name="loginForm"
        className="login-form"
        initialValues={{ remember: true }}
        onFinish={onFinish}
      >
        <Form.Item
          name="email"
          rules={[{ required: true, message: 'Please enter your email!!!' }]}
        >
          <Input
            prefix={<UserOutlined className="site-form-item-icon" />}
            type={"email"}
            placeholder="Enter email..."
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[{ required: true, message: 'Please enter your Password!!!' }]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Enter password..."
          />
        </Form.Item>

        <Form.Item className='formItem'>
          <Button type="primary" htmlType="submit" className="login-form-button">
            Log in
          </Button>
          <br />
          <br />
          Or <Link to={Routes.signUp}>Sign up!</Link>
        </Form.Item>
      </Form>
    </div>
  );
};

export default Login;
