import React, {useState} from 'react';
import {Card, Button, Modal} from "antd";
import {EditOutlined, DeleteOutlined} from '@ant-design/icons';
import '../css/base.css';
import TripService from "../service/TripService";
import CreateTripContainer from "./CreateTripContainer";
import {toast} from "react-toastify";
import moment from "moment";

const TripCard = (props) => {
  const {tripData, updateTripData} = props;
  const [tab, setTab] = useState('destination');
  const [openModal, setOpenModal] = useState(false);

  const toggleModal = () => {
    setOpenModal(!openModal);
  }

  const TripProperties = [
    'destination',
    'id',
    'comment',
    'userId',
    'startDate',
    'endDate'
  ];

  const tabList = [
    {
      key: TripProperties[0],
      tab: 'Destination',
    },
    {
      key: TripProperties[1],
      tab: 'Id',
    },
    {
      key: TripProperties[2],
      tab: 'Comment',
    },
    {
      key: TripProperties[3],
      tab: 'UserId',
    },
    {
      key: TripProperties[4],
      tab: 'StartDate',
    },
    {
      key: TripProperties[5],
      tab: 'EndDate',
    }
  ];

  const deleteTrip = async () => {
    const [data, err] = await TripService.deleteTrip(tripData.id);

    if(data) {
      await updateTripData();
      toast.success('Trip Deleted Successfully')
    } else {
      toast.error(err);
    }
  }

  const onFormUpdate = async (formData) => {
    formData['startDate'] = moment(
      formData.startDate
    ).format('YYYY-MM-DD');
    formData['endDate'] = moment(
      formData.endDate
    ).format('YYYY-MM-DD');

    const [data, err] = await TripService.updateTrip(tripData.id, formData);
    if(data) {
      updateTripData();
      setOpenModal(false);
      toast.success('Trip Updated Successfully!!!');
    } else {
      toast.error(err);
    }
  }

  const contentList = {
    destination: <p style={{float: 'left'}}>{tripData.destination}</p>,
    id: <p>{tripData.id}</p>,
    comment: <p>{tripData.comment ? tripData.comment: 'N/A'}</p>,
    userId: <p>{tripData.userId}</p>,
    startDate: <p>{tripData.startDate}</p>,
    endDate: <p>{tripData.endDate}</p>
  };

  const onTabChange = (key) => {
    setTab(key);
  };

  return (
    <div className='shadow-card'>
      <Card
        size={"small"}
        key={tripData.id}
        style={{
          width: '80%',
          backgroundColor: '#E0E0E0',
          borderRadius: 10,
        }}
        title={<strong>Trip to {tripData.destination}</strong>}
        onTabChange={onTabChange}
        extra={
          <>
            <Button
              type="primary"
              onClick={toggleModal}
              icon={<EditOutlined/>}>
              Edit
            </Button>
            &nbsp;
            <Button
              danger
              type="primary"
              onClick={deleteTrip}
              icon={<DeleteOutlined />}>
              Delete
            </Button>
          </>
        }
        tabList={tabList}>
        {contentList[tab]}
      </Card>

      <Modal
        title="Edit Trip Details"
        visible={openModal}
        onOk={toggleModal}
        footer={null}
        onCancel={toggleModal}
        okButtonProps={{ disabled: true }}
        cancelButtonProps={{ disabled: true }}>
        <CreateTripContainer
          edit={true}
          formInitialData={tripData}
          onSubmitForm={onFormUpdate}
        />
      </Modal>
    </div>
  );
};

export default TripCard;
