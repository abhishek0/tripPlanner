import {
  CombinedState,
  combineReducers,
  createStore,
  Dispatch,
  Reducer
} from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import UserReducer from './user/UserReducer';
import UserActionDispatcher from './user/UserActionDispatcher';
import { UserDispatchAction, UserState } from './user/types';

export interface RootState {
  user: UserState;
}

const RootReducer: Reducer<CombinedState<RootState>> = combineReducers<
  RootState
  >({
  user: UserReducer
});

export type DispatchAction = UserDispatchAction;

const persistConfig = {
  key: 'authType',
  storage
};
const persistedReducer = persistReducer(persistConfig, RootReducer);

const Store = createStore<RootState, DispatchAction, null, null>(
  persistedReducer
);
const Persistor = persistStore(Store);

export const InitializeDispatchers = (dispatch: Dispatch<DispatchAction>) => {
  UserActionDispatcher.dispatch = dispatch as Dispatch<UserDispatchAction>;
};
export { Persistor, Store };
