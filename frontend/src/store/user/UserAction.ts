import UserActionDispatcher from "./UserActionDispatcher";
import UserService from "../../service/UserService";
import { IUser } from '../../models/user.models';

export default class UserAction {
  static async loginUser(
    email: string,
    password: string
  ): Promise<[IUser, string]> {
    UserActionDispatcher.dispatchUserLoading();
    try {
      const [data, jwtToken, err]: [
        IUser,
        string,
        string
      ] = await UserService.loginUser(email, password);

      if(data) {
        localStorage.setItem('jwt', jwtToken);
        UserActionDispatcher.dispatchLoginSuccess(data, jwtToken);
      } else {
        UserActionDispatcher.dispatchLoginFailure();
        UserActionDispatcher.dispatchUserError(err);
      }
      return [data as IUser, err];
    } catch(err) {
      UserActionDispatcher.dispatchUserError('Something went wrong!!!');
      return [undefined, 'Something went wrong'];
    }
  }

  static async logoutUser() {
    localStorage.setItem('jwt', '');
    UserActionDispatcher.dispatchLogoutUser();
    return ['Logged out Successfully', ''];
  }
};
