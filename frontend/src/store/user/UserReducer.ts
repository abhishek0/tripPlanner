import produce, { Draft } from 'immer';
import { Reducer } from 'redux';
import { UserActionTypes, UserDispatchAction, UserState } from './types';
import { IUser } from '../../models/user.models';

const initialState: UserState = {
  loggedIn: 'notattempted',
  loggedInUser: {} as IUser,
  userDetails: {} as IUser,
  userList: [] as IUser[],
  isLoading: false,
  error: '',
  jwtToken: '',
  successMessage: '',
  page: 0
};

const UserReducer: Reducer<UserState, UserDispatchAction> = (
  state = initialState,
  action
): UserState =>
  produce<UserState>(state, (draft: Draft<Partial<UserState>>) => {
    draft.isLoading = false;
    draft.error = '';
    draft.successMessage = '';
    switch (action.type) {
      case UserActionTypes.FETCH_USER_DETAILS_ACTION: {
        draft.successMessage = action.payload.successMessage;
        draft.userDetails = action.payload.userDetails;
        break;
      }

      case UserActionTypes.FETCH_USER_LIST_ACTION: {
        if (action.payload.page === 1) draft.userList = action.payload.userList;
        else {
          if (draft.userList && action.payload.userList)
            draft.userList.push(...action.payload.userList);
        }
        break;
      }
      case UserActionTypes.LOGIN_ACTION_FAILURE: {
        draft.loggedIn = 'failed';
        draft.jwtToken = '';
        break;
      }

      case UserActionTypes.LOGIN_ACTION_SUCCESS: {
        draft.loggedIn = action.payload.loggedIn;
        draft.loggedInUser = action.payload.loggedInUser;
        draft.jwtToken = action.payload.jwtToken;
        break;
      }

      case UserActionTypes.USER_ERROR_ACTION: {
        draft.error = action.payload.error;
        break;
      }
      case UserActionTypes.USER_LOADING_ACTION: {
        draft.isLoading = true;
        break;
      }
      case UserActionTypes.USER_SUCCESS_ACTION: {
        draft.successMessage = action.payload.successMessage;
        break;
      }

      case UserActionTypes.USER_LOGOUT_ACTION: {
        draft.loggedIn = 'failed';
        draft.loggedInUser = {} as IUser;
        break;
      }
      default:
        break;
    }
  });

export default UserReducer;
