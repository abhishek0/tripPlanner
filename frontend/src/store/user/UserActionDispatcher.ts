import { UserActionTypes, UserDispatchAction } from './types';
import { Dispatch } from 'redux';
import { IUser } from '../../models/user.models';

export default class UserActionDispatcher {
  static dispatch: Dispatch<UserDispatchAction>;

  static dispatchLoginSuccess(user: IUser, jwtToken: string) {
    this.dispatch({
      type: UserActionTypes.LOGIN_ACTION_SUCCESS,
      payload: { loggedIn: 'success', loggedInUser: user, jwtToken }
    });
  }

  static dispatchLoginFailure() {
    this.dispatch({
      type: UserActionTypes.LOGIN_ACTION_FAILURE,
      payload: { loggedIn: 'failed', loggedInUser: {} as IUser, jwtToken: '' }
    });
  }

  static dispatchFetchUserDetails(user: IUser) {
    this.dispatch({
      type: UserActionTypes.FETCH_USER_DETAILS_ACTION,
      payload: { userDetails: user }
    });
  }

  static dispatchUserLoading() {
    this.dispatch({
      type: UserActionTypes.USER_LOADING_ACTION,
      payload: {}
    });
  }

  static dispatchUserError(error: string) {
    this.dispatch({
      type: UserActionTypes.USER_ERROR_ACTION,
      payload: { error }
    });
  }

  static dispatchUserSuccess(message: string) {
    this.dispatch({
      type: UserActionTypes.USER_SUCCESS_ACTION,
      payload: { successMessage: message }
    });
  }

  static dispatchLogoutUser() {
    this.dispatch({
      type: UserActionTypes.USER_LOGOUT_ACTION,
      payload: {}
    });
  }
}
