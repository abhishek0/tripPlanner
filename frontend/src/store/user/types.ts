import { Action } from 'redux';
import { LoggedIn, IUser } from '../../models/user.models';

export enum UserActionTypes {
  LOGIN_ACTION_SUCCESS = 'LOGIN_ACTION_SUCCESS',
  LOGIN_ACTION_FAILURE = 'LOGIN_ACTION_FAILURE',

  FETCH_USER_DETAILS_ACTION = 'FETCH_USER_DETAILS_ACTION',
  FETCH_USER_LIST_ACTION = 'FETCH_USER_LIST_ACTION',
  USER_ERROR_ACTION = 'USER_ERROR_ACTION',
  USER_LOADING_ACTION = 'USER_LOADING_ACTION',
  USER_SUCCESS_ACTION = 'USER_SUCCESS_ACTION',
  USER_LOGOUT_ACTION = 'USER_LOGOUT_ACTION'
}

export interface UserState {
  loggedIn: LoggedIn;
  loggedInUser: IUser;
  userDetails: IUser;
  userList: IUser[];
  isLoading: boolean;
  error: string;
  jwtToken: string;
  successMessage: string;
  page: number;
}

export interface UserDispatchAction extends Action<UserActionTypes> {
  payload: Partial<UserState>;
}
