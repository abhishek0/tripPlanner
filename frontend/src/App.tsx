import React, {useEffect} from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Routes } from './route/Routes';
import Home from './components/Home';
import Login from "./components/Login";
import Signup from "./components/Signup";
import CreateTripContainer from "./components/CreateTripContainer";
import TripContainer from "./components/TripContainer";
import UserContainer from './components/UserContainer';
import ProtectedRoute from "./route/ProtectedRoute";
import './App.css';
import 'react-toastify/dist/ReactToastify.css';
import {ToastContainer} from "react-toastify";
import Navbar from "./components/Navbar";
import {DispatchAction, InitializeDispatchers, Store} from './store';
import { useDispatch } from 'react-redux';
import { Dispatch } from 'redux';
import { Provider } from 'react-redux';

function App() {
  const dispatch = useDispatch();
  InitializeDispatchers(dispatch);
  useEffect(() => {
    InitializeDispatchers(dispatch);
  }, [dispatch]);
  return (
    <div className="App">
      <BrowserRouter>
        <Navbar/>
        <Switch>
          <Route
            exact
            path={Routes.home}
            component={Home}>
          </Route>

          <Route
            exact
            path={Routes.login}
            component={Login}>
          </Route>

          <Route
            exact
            path={Routes.signUp}
            component={Signup}>
          </Route>

          <ProtectedRoute
            path={Routes.createTrip}
            key={Routes.createTrip}
            Component={CreateTripContainer}
            exact
          />

          <ProtectedRoute
            path={Routes.trips}
            key={Routes.trips}
            Component={TripContainer}
            exact
          />

          <ProtectedRoute
            path={Routes.users}
            key={Routes.users}
            Component={UserContainer}
            exact
          />
        </Switch>
      </BrowserRouter>
      <ToastContainer />
    </div>
  );
}

export default App;
