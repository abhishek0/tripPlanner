## Main technologies used

- [React](https://reactjs.org/docs/getting-started.html)

> React is a JavaScript library for building user interfaces.

- [Redux](http://redux.js.org/)

## Running the project

- Clone this project
```
git clone < project-url.git >
```

- [Install NodeJS](https://nodejs.org/en/) on your computer.

- [Install yarn](https://yarnpkg.com/en/docs/install) on your computer
> Yarn is a dependency manager built by facebook and google. It is a more efficient and reliable (thanks to yarn.lock) alternative of npm.

## How to run

> Backend
- cd ./backend
- nvm use
- yarn install
- yarn seed
- yarn start

> Frontend
- cd ./frontend
- nvm use
- yarn install
- yarn start
